import { Button, Col, Container, Input, Row } from "reactstrap"
import avatar from "./500x500.png"
function Form () {
    return (
        <Container className="mt-5">
            <Row><h3 className="text-center">HỒ SƠ NHÂN VIÊN</h3></Row>
            <Row>
                <Col>
                    <Row className="mt-2">
                        <Col sm="2">Họ và tên</Col>
                        <Col ><Input></Input></Col>
                    </Row>
                    
                    <Row className="mt-2">
                        <Col sm="2">Ngày sinh</Col>
                        <Col ><Input></Input></Col>
                    </Row>
                    <Row className="mt-2">
                        <Col sm="2">Số điện thoại</Col>
                        <Col ><Input></Input></Col>
                    </Row>
                    <Row className="mt-2">
                        <Col sm="2">Giới tính</Col>
                        <Col ><Input></Input></Col>
                    </Row>
                </Col>
                <Col sm="2">
                    <img className="img-thumbnail" src={avatar}></img>
                </Col>
            </Row>
            <Row>
                <Row className="mt-2">
                    <Col sm="1">Công việc</Col>
                    <Col className="ms-5"><Input type="textarea" className="ms-4" rows="5"></Input></Col>
                </Row>
            </Row>
            <Row className="mt-2">
                <Col className="text-end">
                    <Button color="success" >Chi tiết</Button>
                    <Button color="success" className="ms-2">Kiểm tra</Button>
                </Col>
            </Row>
        </Container>
    )
}

export default Form